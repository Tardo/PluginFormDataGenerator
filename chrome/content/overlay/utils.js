﻿function assert(condition, message) { 
	if (!condition) { 
		throw message || "Assertion failed"; 
	} 
}

function setDisabledElements(elementsId, state) {
	for (var i=0; i<elementsId.length; i++) {
		var domElement = document.getElementById(elementsId[i]);
		if (domElement)
			domElement.disabled = state;
	}
}

function setHiddenElements(elementsId, state) {
	for (var i=0; i<elementsId.length; i++) {
		var domElement = document.getElementById(elementsId[i]);
		if (domElement)
			domElement.hidden = state;
	}
}

function isNumber(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}

function normalizeStr(str) {
	var from = "ÃÀÁÄÂÈÉËÊÌÍÏÎÒÓÖÔÙÚÜÛãàáäâèéëêìíïîòóöôùúüûÑñÇç",
		to   = "AAAAAEEEEIIIIOOOOUUUUaaaaaeeeeiiiioooouuuunncc",
		mapping = {};
 
	for(var i=0, j=from.length; i<j; i++)
		mapping[ from.charAt( i ) ] = to.charAt( i );
 

	var ret = [];
	for (var i=0, j=str.length; i<j; i++) {
		var c = str.charAt(i);
		if (mapping.hasOwnProperty(str.charAt(i)))
			ret.push(mapping[c]);
		else
			ret.push(c);
	}
	  
	return ret.join('');
}

function getIntRandomArbitary(min, max) {
    return Math.floor(Math.random() * (max - min) + min);
}

function getFloatRandomArbitary(min, max) {
    return (Math.random() * (max - min) + min).toFixed(3);
}