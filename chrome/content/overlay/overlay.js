﻿/*
	Generador Datos Formulario - Extension
	Alexandre Díaz - 2014
	
	TODO: Actualizar plugin para trabajar con HTML5
*/

function getValidationLetterDNINIE(dniNie){
	if (dniNie.charAt(0) == 'X')
		dniNie = "0"+dniNie.substring(1);
	else if (dniNie.charAt(0) == 'Y')
		dniNie = "1"+dniNie.substring(1);
	else if (dniNie.charAt(0) == 'Z')
		dniNie = "2"+dniNie.substring(1);
		
	return ("TRWAGMYFPDXBNJZSQVHLCKE").charAt(dniNie % 23);
}

function getValidationLetterCIF(cif){	
	var digits = cif.substr(1);
	var acumA = 0;
	var acumB = 0;
	
	for (var i=0; i<7; i++) {
		if ((i+1)%2 == 0) { acumA += parseInt(digits.charAt(i)); } 
		else {
			var num = parseInt(digits.charAt(i))*2;
			if (num.toString().length == 2)
				acumB += parseInt(num.toString().charAt(0))+parseInt(num.toString().charAt(1));
			else 
				acumB += parseInt(num);
		}
	}
	
	var acumC = acumA+acumB;
	var digAcumC = (acumC<10?acumC:parseInt(acumC.toString().charAt(1)));
	if (digAcumC > 0) ctrl = 10-digAcumC;
	else ctrl = 0;
	
	var res = '';
	if (cif.charAt(0) == 'N' || cif.charAt(0) == 'P' || cif.charAt(0) == 'Q' || cif.charAt(0) == 'R' || cif.charAt(0) == 'S' || cif.charAt(0) == 'W')
		res = "JABCDEFGHI".charAt(ctrl);
	else
		res = ctrl;
		
	return res;
}

var FormDataGenerator = {
	/** METODOS GENERALES **/
	init: function(event) {
		try {
			window.removeEventListener("load", FormDataGenerator.init, false);
			if (gBrowser) gBrowser.addEventListener("DOMContentLoaded", FormDataGenerator.onPageLoad, false)
			
			// Eventos MenuContext
			var contextMenu = document.getElementById("contentAreaContextMenu");
			if (contextMenu) contextMenu.addEventListener("popupshowing", FormDataGenerator.checkStateMenu, false);
	
			// Crear MenuItems del Menu Telefono
			var menuPhone = document.getElementById("generate-phone-menu");
			var menuPopup = (menuPhone.firstElementChild || menuPhone.firstChild);
			for (var key in phonePrefix) {
				var menuItem = document.createElement("menuitem");
				menuItem.setAttribute("label", key);
				menuItem.setAttribute("oncommand", "FormDataGenerator.getRandPhone(document.popupNode, \""+key+"\")");
				menuPopup.appendChild(menuItem);
			}

		} catch (exception) { 
			// Ignorar
		}
	},
	
	onPageLoad: function(event){
		// TODO
	},
	
	keyboardShortcuts: function(event){
		if (event.shiftKey && event.altKey) {
			var elm = document.commandDispatcher.focusedWindow.document.activeElement;
			var charKey = String.fromCharCode(event.which).toUpperCase();
			
			switch(charKey) {
				case 'N': FormDataGenerator.getRandDNI(elm); break;
				case 'I': FormDataGenerator.getRandNIE(elm); break;
				case 'F': FormDataGenerator.getRandCIF(elm); break;
				case 'G': FormDataGenerator.getLetterDNI(elm); break;
				
				case 'H': FormDataGenerator.getRandMenName(elm); break;
				case 'B': FormDataGenerator.getRandWomanName(elm); break;
				case 'R': FormDataGenerator.getRandLastName(elm); break;
				
				case 'P': FormDataGenerator.getRandDate(elm,Constants.DATE_TYPE_BIRTH); break;
				
				case 'O': FormDataGenerator.getRandPhone(elm,''); break;
				
				case 'X': FormDataGenerator.getRandMail(elm); break;
				
				case 'Z': FormDataGenerator.selectRandItem(elm); break;
			}
		}
	},
	
	checkStateMenu: function(event){
		var elementsId=["formdatagenerator-menu","generate-identify-menu","generate-names-menu","generate-numbers-menu","generate-date-menu","generate-phone-menu","generate-mail"];
		if (document.popupNode.tagName.toLowerCase() !== "input" 
		|| (document.popupNode.tagName.toLowerCase() === "input" && (document.popupNode.type.toLowerCase() === "button" || document.popupNode.type.toLowerCase() === "submit"))) 
			setHiddenElements(elementsId, true);
		else 
			setHiddenElements(elementsId, false);
			
		var selectRandComboMenuItem = document.getElementById("select-random-combo-menuitem");
		if (document.popupNode.tagName.toLowerCase() !== "select") setHiddenElements(["select-random-combo-menuitem"], true);
		else setHiddenElements(["formdatagenerator-menu","select-random-combo-menuitem"], false);
	},
	
	/** MENU IDENTIFICADORES **/
	checkDisabledMenuIdentify: function(event){
		if (!gContextMenu.onTextInput || document.popupNode.value.length != 8 || !isNumber(document.popupNode.value))
			setDisabledElements(["generate-letdni-menuitem"], true);
		else
			setDisabledElements(["generate-letdni-menuitem"], false);
	},
	
	getRandDNI: function(elm){
		assert(elm.type.toLowerCase() === "text");
		
		var genNum = "";
		for (var i=0; i<8; i++)
			genNum += getIntRandomArbitary(0, 9).toString();
		
		elm.value = genNum+getValidationLetterDNINIE(genNum);
	},
	
	getRandNIE: function(elm){
		assert(elm.type.toLowerCase() === "text");
		
		var genNum = "";
		var letId = getIntRandomArbitary(0, 2);
		if (letId == 0) genNum = "X";
		else if (letId == 1) genNum = "Y";
		//else { genNum = "Z"; }
		
		for (i=0; i<7; i++)
			genNum += getIntRandomArbitary(0, 9).toString();
		
		elm.value = genNum+getValidationLetterDNINIE(genNum);
	},
	
	getRandCIF: function(elm){
		var provCode = [
			"01","02","03","53","54","04","05","06","07","57",
			"08","58","59","60","61","62","63","64","65","66",
			"68","09","10","11","72","12","13","14","56","15",
			"70","16","17","55","18","19","20","21","22","23",
			"24","25","26","27","28","78","79","80","81","82",
			"83","84","85","86","29","92","93","30","73","31",
			"71","32","33","74","34","35","76","36","27","94",
			"37","38","75","39","40","41","90","91","42","43",
			"77","44","45","46","96","97","98","47","48","95",
			"49","50","99","51","52"
		];
		var genNum = "ABCDEFGHJNPQRSUVW".charAt(getIntRandomArbitary(0, 16));
		genNum += provCode[getIntRandomArbitary(0, provCode.length-1)];
		
		for (i=0; i<5; i++)
			genNum += getIntRandomArbitary(0, 9).toString();
		
		elm.value = genNum+getValidationLetterCIF(genNum);
	},

	getLetterDNI: function(elm){
		assert(elm.type.toLowerCase() === "text" && elm.value.length == 8 && isNumber(elm.value));
		
		var tempValue = elm.value;
		elm.value += getValidationLetterDNINIE(tempValue);
	},
	
	/** MENU NOMBRES **/
	getRandMenName: function(elm){
		assert(elm.type.toLowerCase() === "text");
		
		elm.value = namesMen[getIntRandomArbitary(0, namesMen.length-1)];
	},
	
	getRandWomanName: function(elm){
		assert(elm.type.toLowerCase() === "text");
		
		elm.value = namesWoman[getIntRandomArbitary(0, namesMen.length-1)];
	},
	
	getRandLastName: function(elm){
		assert(elm.type.toLowerCase() === "text");
		
		elm.value = lastNames[getIntRandomArbitary(0, lastNames.length-1)];
	},
	
	getRandMenFullName: function(elm){
		assert(elm.type.toLowerCase() === "text");
		
		elm.value = namesMen[getIntRandomArbitary(0, namesMen.length-1)] + " " + lastNames[getIntRandomArbitary(0, lastNames.length-1)] + " " + lastNames[getIntRandomArbitary(0, lastNames.length-1)];
	},
	
	getRandWomanFullName: function(elm){
		assert(elm.type.toLowerCase() === "text");
		
		elm.value = namesWoman[getIntRandomArbitary(0, namesMen.length-1)] + " " + lastNames[getIntRandomArbitary(0, lastNames.length-1)] + " " + lastNames[getIntRandomArbitary(0, lastNames.length-1)];
	},
	
	/** MENU MAILS **/
	getRandMail: function(elm){
		assert(elm.type.toLowerCase() === "text");
		
		var genId = getIntRandomArbitary(0, 2);
		var name = "";
		if (genId == 0) name = namesMen[getIntRandomArbitary(0, namesMen.length-1)];
		else if (genId == 1) name = namesWoman[getIntRandomArbitary(0, namesMen.length-1)];
		
		elm.value = normalizeStr(name.toLowerCase())+"@correo.es";
	},
	
	/** MENU NUMEROS **/
	getRandFloatNumber: function(elm){
		assert(elm.type.toLowerCase() === "text");
		
		elm.value = getFloatRandomArbitary(0, 9999);
	},
	
	getRandIntNumber: function(elm){
		assert(elm.type.toLowerCase() === "text");
		
		elm.value = getIntRandomArbitary(0, 9999);
	},
	
	/** MENU FECHA/HORA **/
	getRandDate: function(elm, type){
		assert(elm.type.toLowerCase() === "text");
		
		var currentDate = new Date();
		var maxDay = 20; // TODO: Calcular maximo dia por mes
		var maxMonth = 12;
		var maxYear = currentDate.getFullYear()+100;
		var minYear = currentDate.getFullYear()-100;
		
		if (type == Constants.DATE_TYPE_CURRENT_YEAR) {
			maxYear = currentDate.getFullYear();
			minYear = currentDate.getFullYear();
		} else if (type == Constants.DATE_TYPE_YOUNG_BIRTH) {
			maxDay = currentDate.getDate();
			maxMonth = currentDate.getMonth()+1;
			maxYear = currentDate.getFullYear();
			minYear = currentDate.getFullYear()-18;
		} else if (type == Constants.DATE_TYPE_BIRTH) {
			maxDay = currentDate.getDate();
			maxMonth = currentDate.getMonth()+1;
			maxYear = currentDate.getFullYear();
			minYear = currentDate.getFullYear()-128;
		}
		
		var day = getIntRandomArbitary(1, maxDay);
		var month = getIntRandomArbitary(1, maxMonth);
		var year = getIntRandomArbitary(minYear, maxYear);
		elm.value = (day<10?"0"+day:day)+"/"+(month<10?"0"+month:month)+"/"+year;
	},
	
	getRandHour: function(elm, type){
		assert(elm.type.toLowerCase() === "text");
		
		var maxHour = 24;
		var maxMin = 59;
		
		if (type == Constants.HOUR_TYPE_12H)
			maxHour = 12;
		
		var hours = getIntRandomArbitary(0, maxHour);
		var mins = getIntRandomArbitary(0, maxMin);
		elm.value = (hours<10?"0"+hours:hours)+":"+(mins<10?"0"+mins:mins);
	},
	
	/** MENU TELEFONO **/
	getRandPhone: function(elm, prefixKey){
		assert(elm.type.toLowerCase() === "text" || elm.type.toLowerCase() === "phone");
		
		var genPhone = "";
		if (prefixKey.length === 0)
			genPhone = phonePrefix[Object.keys(phonePrefix)[getIntRandomArbitary(0, Object.keys(phonePrefix).length-1)]];
		else
			genPhone = phonePrefix[prefixKey];
			
		for (var i=0; i<6; i++)
			genPhone += getIntRandomArbitary(0, 9);
		
		elm.value = genPhone;
	},
	
	/** MENU SELECCIONAR ITEM ALEATORIO **/
	selectRandItem: function(elm){
		assert(elm.tagName.toLowerCase() === "select");
	
		var genNum = getIntRandomArbitary(0, elm.options.length);
		elm.selectedIndex = genNum;

		var evt = document.createEvent("HTMLEvents");
		evt.initEvent("change", true, true);
		elm.dispatchEvent(evt);
	}
	
};

/** EVENTOS **/
window.addEventListener("load", FormDataGenerator.init, false);
window.addEventListener("keypress", FormDataGenerator.keyboardShortcuts, false);